package org.demo.knight;


import org.demo.knight.knights.*;

public class PlainMain1 {

	public static void main(String[] args) {
		new LadiesKnight().embarkOnQuest();
		new HandyKnight(new ErrandQuest()).embarkOnQuest();
	}
}
