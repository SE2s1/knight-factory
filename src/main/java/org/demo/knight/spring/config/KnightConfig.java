package org.demo.knight.spring.config;

import org.demo.knight.knights.HandyKnight;
import org.demo.knight.knights.Knight;
import org.demo.knight.knights.Quest;
import org.demo.knight.knights.ErrandQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KnightConfig {
    @Bean
    public Knight knight(Quest quest) {
        return new HandyKnight(quest);
    }

    @Bean
    public Quest quest() {
        return new ErrandQuest();
    }
}
