package org.demo.knight;


import org.demo.knight.factory.BeanFactory;
import org.demo.knight.factory.MedievalFactory;
import org.demo.knight.knights.Knight;

public class FactoryMain2 {

	public static void main(String[] args) {
		BeanFactory context = new MedievalFactory();
		Knight johan = context.getKnight("handy");
		johan.embarkOnQuest();

	}
}
