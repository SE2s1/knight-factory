package org.demo.knight.factory;


import org.demo.knight.knights.*;

public class MedievalFactory implements BeanFactory {


	@Override
	public Knight getKnight(String type) {
		switch (type.toLowerCase()) {
			case "ladies":
				return new LadiesKnight();
			case "handy":
			default:
				return new HandyKnight(new ErrandQuest());
		}
	}

}
