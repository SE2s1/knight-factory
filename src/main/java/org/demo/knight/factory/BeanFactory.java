package org.demo.knight.factory;

import org.demo.knight.knights.Knight;

/**
 * @author Jan de Rijke.
 */
public interface BeanFactory {
	Knight getKnight(String type);
}
