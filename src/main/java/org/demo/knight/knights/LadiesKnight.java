package org.demo.knight.knights;


public class LadiesKnight implements Knight {
	private RescueDamselQuest quest;

	public LadiesKnight() {
		this.quest = new RescueDamselQuest();
	}

	public void embarkOnQuest() {
		quest.embark();
	}
}
