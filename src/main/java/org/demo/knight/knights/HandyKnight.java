package org.demo.knight.knights;


public class HandyKnight implements Knight {
	private Quest quest;

	public HandyKnight(Quest quest) {
		this.quest = quest;
	}

	@Override
	public void embarkOnQuest() {
		quest.embark();
	}
}


