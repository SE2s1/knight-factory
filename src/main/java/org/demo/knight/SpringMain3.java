package org.demo.knight;

import org.demo.knight.spring.config.KnightConfig;
import org.demo.knight.knights.Knight;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringMain3 {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(KnightConfig.class,args);
		context.getBean(Knight.class).embarkOnQuest();
		context.close();	}
}
